import os
import string
import random

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy

from django_site.settings import BASE_DIR

THUMBNAIL_ROOT = os.path.join(BASE_DIR, 'media/thumbnails')
THUMBNAIL_URL = '/media/thumbnails/'


def generate_new_filename(file_name):
    """Generate new file name with ascii + digits and add it to the file extension.
    """
    _, ext = os.path.splitext(file_name)
    name = ''.join(random.choice(string.ascii_lowercase + string.digits) 
        for x in xrange(16)
    )
    return name + ext