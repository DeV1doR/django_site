import os
import json
from django.shortcuts import get_object_or_404, redirect
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from django_site.settings import BASE_DIR
from django.views.generic import View, TemplateView, ListView, RedirectView
from django.views.generic.edit import FormView, CreateView, DeleteView
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth.models import User

from cms.models import Project
from cms.forms import LoginForm, UploadFileForm
from bl.view_helpers import generate_new_filename
from bl.mixins import AjaxableResponseMixin, LoginRequiredMixin


class LoginView(AjaxableResponseMixin, FormView):

    template_name = 'cms/auth.html'
    form_class = LoginForm
    success_url = reverse_lazy('cms:admin-main')

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['redirect_url'] = self.success_url
        return context

    def form_valid(self, form):
        # import pdb; pdb.set_trace()
        login(self.request, form.get_user())
        return super(FormView, self).form_valid(form)


class LogoutView(LoginRequiredMixin, RedirectView):

    url = '/admin/login/'

    def post(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class AdminView(LoginRequiredMixin, TemplateView):

    template_name = 'cms/pages/main.html'


class ProjectView(LoginRequiredMixin, ListView):

    template_name = 'cms/pages/projects.html'
    model = Project
    context_object_name = 'projects'


class ProjectAddView(LoginRequiredMixin, AjaxableResponseMixin, CreateView):

    template_name = 'cms/pages/projects.add.html'
    form_class = UploadFileForm
    success_url = reverse_lazy('cms:projects')

    def form_valid(self, form):
        # import pdb; pdb.set_trace()
        project = form.save(commit=False)
        project.user = User.objects.get(id=self.request.user.id)
        project.photo.name = generate_new_filename(project.photo.name)
        project.save()
        return super(ProjectAddView, self).form_valid(form)


class ProjectDeleteView(LoginRequiredMixin, DeleteView):

    model = Project
    success_url = reverse_lazy('cms:projects-main')

    def delete_photo_from_path(self, *args, **kwargs):
        try:
            os.remove(self.object.photo.path)
        except AttributeError as e:
            return JsonResponse(data={"error": "Photo not found"}, status=404)        

    def delete(self, request, *args, **kwargs):
        # import pdb; pdb.set_trace()
        self.object = self.get_object()
        self.delete_photo_from_path()
        self.object.delete()
        return JsonResponse(
            data={"success": "Project %(pk)d successfully deleted" % {"pk": int(self.kwargs['pk'])}}, 
            status=200
        )


# @login_required(login_url='/')
# def delete_project(request):
#     if request.is_ajax():
#         if request.method == 'DELETE':
#             data = json.loads(request.body)
#             prj = Project.objects.get(id=data['prj_id'])
#             path = str(prj.photo_name.path)
#             prj.delete()
#             os.remove(path)
#             return HttpResponse()