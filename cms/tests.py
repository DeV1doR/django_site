from django.test import TestCase
from django.test.client import RequestFactory, Client
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import User, AnonymousUser
from django.contrib.sessions.middleware import SessionMiddleware

from cms.views import (
    LoginView
)
from cms.forms import (
    LoginForm
)

FAKE_USERNAME = "Alex"
FAKE_EMAIL = "gertic@tset.com"
FAKE_PASSWORD = "glasska"

def add_session_to_request(request):
    middleware = SessionMiddleware()
    middleware.process_request(request)
    request.session.save()


class LoginTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.fake_path = reverse_lazy('cms:login')
        self.redirect_url = reverse_lazy('cms:admin-main')
        self.user = User.objects.create_user(username=FAKE_USERNAME, 
                                             email=FAKE_EMAIL, 
                                             password=FAKE_PASSWORD)

    def test_created_user(self):
        # import pdb; pdb.set_trace()
        check_user = User.objects.get(email=FAKE_EMAIL)
        self.assertEqual(self.user, check_user)

    def test_login_get_200_and_get_redirect_url_in_input(self):
        # HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        request = self.factory.get(self.fake_path)
        view = LoginView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.redirect_url, response.context_data['redirect_url'])

    def test_login_ajax_post(self):
        request = self.factory.post(
            path=self.fake_path,
            data={"username": FAKE_USERNAME, "password": FAKE_PASSWORD},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        add_session_to_request(request)
        view = LoginView.as_view()
        response = view(request)
        import ipdb; ipdb.set_trace()
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.get('location'), reverse_lazy('cms:admin-main'))
        self.assertRedirects(response, self.redirect_url, status_code=302)

    def tearDown(self):
        User.objects.get(email=FAKE_EMAIL).delete()

    # def test_login_post_200(self):
    #     fake_data = dict(username='Gena', password='Bukin')
    #     request = self.factory.post(self.fake_path, fake_data)
    #     response = LoginView.as_view()(request)
    #     self.assertEqual(response.status_code, 200)

# class MainTest(unittest.TestCase):
#     def setUp(self):
#         self.factory = RequestFactory()
#         self.fake_path = reverse_lazy('cms:admin-main')

#     def test_main_get_ok(self):
#         request = self.factory.get(self.fake_path)
#         response = AdminView.as_view()(request)
#         self.assertEqual(response.status_code, 200)