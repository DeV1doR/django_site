$(document).ready(function() {
    var getCookie = function(c_name) {
        if (document.cookie.length > 0) {
            c_start = document.cookie.indexOf(c_name + "=");
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1;
                c_end = document.cookie.indexOf(";", c_start);
                if (c_end == -1) c_end = document.cookie.length;
                return unescape(document.cookie.substring(c_start,c_end));
            }
        }
        return "";
    };
    $.ajaxSetup({
        headers: {
            'X-CSRFToken': getCookie("csrftoken")
        }
    });
    var saved_errors = [];
    var login_request = function() {
        $.ajax({
            method: $("#signIn").attr("method"),
            url: $("#signIn").attr("action"),
            cache: false,
            data: {
                username: $("input#username").val(),
                password: $("input#password").val()
            },
            success: function(json) {
                // console.log("Success");
                window.location.href = $("input#main").val();
            },
            error: function(response, data, xhr, errmsg, err) {
                var errors = $.parseJSON(response.responseText);

                for (var i=0; i<saved_errors.length; i++) {
                    $(saved_errors[i]).empty();
                }

                for (var error in errors) {
                    var error_id = "#ajax_error_" + error;
                    saved_errors.push(error_id);
                    $(error_id).append('<div class="form-group">' + errors[error] +'</div>');
                }
                console.log('Ajax post failure');
                console.log(xhr.status);
                console.log(errors);
            }
        });
    };
    $('.delete_project').on("click", function(event) {
        event.preventDefault();
        var form = $(this).parent();
        var prj_id = "#prj_" + form.attr('action').match(/\d+/)[0]
        $.ajax({
            method: "DELETE",
            url: form.attr('action'),
            success: function(data) {
                var msg_div = '<div class="alert alert-success" role="alert">';
                msg_div +=          '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                msg_div +=          '<span>'+data.success+'</span>';
                msg_div +=    '</div>';
                $("#success_delete").append(msg_div);
                $(prj_id).remove();
                // console.log($(prj_id));
                // console.log(data.success);
            },
            error: function(response) {
                console.log(response.responseText);
            }
        });
    });
    $("#signIn").on("submit", function(event) {
        event.preventDefault();
        login_request();
    });
});