from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import gettext as _
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, models as dj
from cms import models as pf


class UploadFileForm(forms.ModelForm):
    class Meta:
        model = pf.Project
        fields = ['title', 'content', 'photo', 'url']
        exclude = ['user']
        labels = {
            'title': _('Select title for the project:'),
            'url': _('Set up URL for the project:'),
            'content': _('Description of the project:'),
            'photo': _('Add photo for the project:'),
        }
        widgets = dict(
            title=forms.TextInput(attrs={'class': 'form-control'}),
            url=forms.TextInput(attrs={'class': 'form-control'}),
            content=forms.Textarea(attrs={'class': 'form-control'}),
        )
        error_messages = dict(
            title=dict(
                required=_('Title is required(max: 25)'),
            ),
            url=dict(required=_('Url is required')),
            content=dict(
                required=_('Content is required(max: no limit)'),
            ),
            photo=dict(required=_('You don\'t select a photo')),
        )  


class LoginForm(AuthenticationForm):
    
    username = forms.CharField(
        label=_("Username"), 
        max_length=254,
        error_messages={
            'required': _('Username is required')
        },
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'id': 'username'
        }),
    )
    password = forms.CharField(
        label=_("Password"), 
        error_messages={
            'required': _('Password is required')
        },
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'id': 'password'
        }),
    )