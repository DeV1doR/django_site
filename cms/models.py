from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    class Meta:
        db_table = 'Project'

    title = models.CharField(max_length=25)
    content = models.TextField()
    photo = models.ImageField()
    url = models.URLField()
    user = models.ForeignKey(User)