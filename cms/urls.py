from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from cms.views import (
    LoginView, 
    LogoutView, 
    AdminView, 
    ProjectView, 
    ProjectAddView,
    ProjectDeleteView
)

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^main/$', AdminView.as_view(), name='admin-main'),
    url(r'^projects/$', ProjectView.as_view(), name='projects'),
    url(r'^projects/add/$', ProjectAddView.as_view(), name='projects-add'),
    url(r'^projects/(?P<pk>[0-9]+)/delete/$', ProjectDeleteView.as_view(), name='projects-delete'),
    # url(r'^projects/(?P<pk>[0-9]+)/update/$', ProjectUpdateView.as_view(), name='projects.update'),
]