from django.test import TestCase, RequestFactory
from django.http import HttpRequest
from portfolio.views import MainPage


class TestMainPage(TestCase):
	rf = RequestFactory()
	fake_path = '/main/'

	def test_get_ok_status_and_title(self):
		request = self.rf.get(self.fake_path)
		response = MainPage.as_view()(request)
		self.assertEqual(response.status_code, 200)
		self.assertIn('Devportfolio', response.content)
