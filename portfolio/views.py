from django.views.generic import ListView
from django.shortcuts import render
from cms.models import Project


class MainPage(ListView):
    template_name = 'portfolio/index.html'
    model = Project
    context_object_name = 'projects'